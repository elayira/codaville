---
templateKey: about-page
title: About Us
meta_title: About Us | Codaville
meta_description: Our mission is to educate the next generation of coders who will use their skills and     
  knowledge to make the world a better place.
about:
  core_values:
    - value: We delight kids and wow parents
    - value: We create fun and a little wackiness
    - value: We pursue growth and learning with coding, gaming, and education
    - value: We build open and honest relationships with communication
    - value: We build a positive team spirit
    - value: We do more with less & think outside the box
    - value: We constantly improve
    - value: We work with diligence and drive with tenacity
    - value: We do what we say we are going to do
    - value: We give back to our communities
  what_we_do:
    title: what we do
    contents:
      - content: CodaKid is an online kids coding academy and tech camp that teaches kids to create games,
          apps, and Minecraft mods using real programming languages and professional tools. CodaKid's online 
          classes are self-paced and include live support from a friendly team of engineers. Since 2016 
          CodaKid has taught nearly 10,000 students to code in 15 countries and growing. CodaKid's camps and 
          classes provide the highest level of kids coding instruction available in the market with a fun, 
          student centered approach and small group attention.
      - content: Our is a goal is to create a generation of young programmers, designers, and entrepreneurs 
          who can build amazing games, apps, and more. Our courses are engineered to teach kids not only how 
          to design and code, but to thoroughly understand the concepts so that they can perform them on 
          their own. By making computer programming incredibly fun and engaging through game design, CodaKid 
          students will develop the motivation and tenacity to learn to code with actual development 
          environments and develop real world skills.
  team:
    caption: our team
    members:
      - name: David Dodge
        title: CEO
        description: David Dodge is a veteran game designer and software architect who is credited on over 30 
          games for the Sony Playstation, various SEGA platforms, and the PC. He is additionally the software 
          architect of Tutorware, a business management software built for tutoring companies. David overseas 
          strategic planning, product development, business development  and marketing for CodaKid. In his 
          free time, David enjoys chasing his three year old daughter Dylan around, playing guitar, traveling,
          and cooking with his wife Lauren. David earned an MBA from Thunderbird Global School of 
          International Management.
        image: /img/team/david-dodge-300x208.jpg
      - name: Lauren Dodge
        title: Director of Operations
        description: Lauren Dodge, JD/MBA, is a seasoned education entrepreneur who grew her last startup
          SurePrep Learning into an Inc 500/5000 company for three years in a row. Lauren directs operations, 
          finance, and human resources at CodaKid. In her free time, she likes playing with her daughter 
          Dylan, cooking, traveling, and scuba diving.
        image: /img/team/lauren-nguyen-2-300x208.jpg
      - name: Andy Boyan
        title: Product Development
        description: Hello everyone, Andy here. And welcome to my bio. Andy is a game developer and teacher 
          at CodaKid. He has developed several courses for CodaKid that are fun and engaging. His favorite 
          programming language is Python and is the teacher for all of CodaKid's Python courses. He can found 
          at CodaKid meowing at the feral cats.
        image: /img/team/BreakoutAndy6-768x432.png
      - name: Austin Larsen
        title: Product Development
        description: Austin is a game developer and seasoned CodaKid teacher who has developed and showcased  
          gaming and design projects. Austin works closely with CodaKid to create fun and engaging courses 
          for CodaKid students. Austin enjoys teaching and being able to help young students understand how 
          fun programming can be. During our summer camps, Austin can be found playing a friendly game of 
          Super Smash Bros. with the students.
        image: /img/team/austinSmall-1-300x200.png
      - name: Matthew Kirstein
        title: Tech Support Lead
        description: Matthew is a self-taught coder who enjoys helping others. At CodaKid, Matthew helps
          online students fix bugs and understand coding concepts. He also helps make sure the computers at 
          CodaKid are up and running all the time. In his free time, he enjoys long walks on the beach and 
          playing World of Warcraft.
        image: /img/team/matthew2-300x190.png
---