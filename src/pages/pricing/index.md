---
templateKey: pricing-page
title: give the gift of codaville
meta_title: Pricing | Codaville
meta_description: >-
  Gift a single course, Course Bundle, or All Access Subscription.
  Gifts can be printed as a certificate or emailed as an e-gift
pricing:
  description: Gift a single course, Course Bundle, or All Access Subscription. Gifts can be printed as a
    certificate or emailed as an e-gift
  heading: CHOOSE YOUR GIFT!
  plans:
    - description: The ultimate gift for young techies. 12 months of CodaKid's award-winning courses. 30+
        courses including upcoming 2019 courses in Machine Learning, 3D Game Development with Unity, Python 
        Basics, and more!
      image: /img/gifts/5.jpg
      plan: 12 Month All Access Subscription
      price: '199'
      id: '1'
    - description: The perfect gift for young techies who love Minecraft. Kids learn real Java while creating 
        amazing custom mods, including a custom axe that shoots fireballs and a repulsion shields that throws 
        enemies into the air! 12-15 hours of student projects.
      plan: Mod Creation Essentials
      price: '49'
      image: /img/gifts/1.jpg
      id: '2'
    - description: A popular gift for Roblox fans who want to publish their own games on Roblox Studio! Kids 
        learn real Lua coding while creating Obbies, Adventure Maps, Racing Games, and Tycoon Games! 12-15 
        hours of student projects.
      plan: Roblox 3D Game Programming
      price: '45'
      image: /img/gifts/2.jpg
      id: '3'
    - description: Double the fun with Minecraft coding and Roblox Game Development. Kids learn Java and Lua, 
        in this exciting Bundle which includes a special bonus course. 25+ Hours of student projects
      plan: Minecraft+Roblox Essentials Bundle
      image: /img/gifts/3.jpg
      price: '65'
      id: '4'
---

