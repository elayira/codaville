---
templateKey: educator-page
title: TRY CODAKID FOR SCHOOLS AND GROUPS
meta_title: Coding For Schools | Codaville
meta_description: Interested in implementing CodaKid’s Coding Program in your school?
educator:
  courses:
    - title: MOD CREATION 1
      caption: WITH MINECRAFT
      image: /img/online_courses/mod-creation-1-large.png
      description: Learn real Java coding online at your own pace with Minecraft®. Make
        incredible custom mods!
    - title: MOD CREATION 2
      caption: WITH MINECRAFT
      image: /img/online_courses/mod-creation-2-large.png
      description: Learn real Java programming by creating three incredible custom 
        dimensions in Minecraft®!
    - title: GAME DEVELOPMENT 1
      caption: WITH ROBLOX
      image: /img/online_courses/game-development-with-roblox-large.png
      description: Kids learning real Lua coding with Roblox!
    - title: GAME PROGRAMMING 1
      caption: WITH JAVASCRIPT
      image: /img/online_courses/game-programming-1-large.png
      description: Learn real JavaScript game programming and make a pro quality Infinite 
        Runner style game!
    - title: GAME PROGRAMMING 2
      caption: WITH JAVASCRIPT
      image: /img/online_courses/game-programming-2-large.png
      description: Kids learn how to build a professional-grade game using JavaScript
    - title: 3D GAME DEVELOPMENT
      caption: WITH UNREAL
      image: /img/online_courses/3d-game-development-with-unreal-large.png
      description: "Kids build a professional quality 3D platform game using Unreal 
        Engine 4"
    - title: DRONE PROGRAMMING 1
      caption: WITH ARDUINO
      image: /img/online_courses/drone-programming-1-large.png
      description: Kids learn real Arduino coding while programming their own Drone
    - title: GAME PROGRAMMING 1
      caption: WITH PYTHON
      image: /img/online_courses/python-fish-eat-fish-large.png
      description: Make a game where you must survive without being eaten by bigger fish
        in Python
    - title: GAME PROGRAMMING 2
      caption: WITH PYTHON
      image: /img/online_courses/game-programming-2-with-python-large.png
      description: Make a game where you must survive while destroying robots in Python
    - title: WEB DEVELOPMENT 1
      caption: WITH HTML/CS
      image: /img/online_courses/web-development-1-large.png
      description: In this online coding course for kids, learn HTML/CSS programming in a 
        fun gamified way while making a real website!
  feature_banner:
    - image: /img/schools-informations/1.png
      caption: Hundreds of hours of award-winning student projects
    - image: /img/schools-informations/2.png
      caption: Real programming languages like Python, JavaScript, Java, and more
    - image: /img/schools-informations/3.png
      caption: Professional tools used at companies like Facebook, Google and Intel.
    - image: /img/schools-informations/4.png
      caption: Full remote support from our online engineering team
    - image: /img/schools-informations/5.png
      caption: Progress tracking
    - image: /img/schools-informations/6.png
      caption: All concepts aligned with CSTA standards
  language_banner:
    caption: Focus on the big picture! Jumpstart your kid's career & ensure future
      success by helping them master the following computer coding languages!
    images: 
      - image: /img/programming/arduino.png
      - image: /img/programming/css.png
      - image: /img/programming/html.png
      - image: /img/programming/java.png
      - image: /img/programming/javascript.png
      - image: /img/programming/lua.png
      - image: /img/programming/python.png
      - image: /img/programming/unreal_engine.png
  promo_banner:
    title: SECURE AND FERPA COMPLIANT
    caption: Student data is secure and owned by your school.
    subcaption: CodaKid is COPPA, FERPA and SOPIPA compliant.
    image: /img/schools_secure.png
    promo_blurbs:
      - image: /img/schools_advantage/1.png
        title: TEXT-BASED CODING
        description: Kids learn the foundations of programming concepts, includin if-then 
          statements, variables, and complex logic.
        icon: /img/schools_advantage/icon_1.png
      - image: /img/schools_advantage/2.png
        title: COMPREHENSIVE CURRICULA
        description: Kids build real games and apps using professional coding languages
        icon: /img/schools_advantage/icon_2.png
      - image: /img/schools_advantage/3.png
        title: CREATIVE TOOLS
        description: Our friendly teachers will help your child through messaging and
          screen share
        icon: /img/schools_advantage/icon_3.png
  inquire_banner:
    - image: /img/schools_image_2.jpg
      caption: Built for Schools that are ready to move past Tynker, Scratch, and Code.org
    - image: /img/team.jpg
      caption: BEST TEACHERS IN THE BUSINESS!
---