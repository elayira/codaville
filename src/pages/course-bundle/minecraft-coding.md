---
templateKey: course-page
title: minecraft coding
meta_title: Mod Creation - The Adventure begins | Codaville
meta_description: CodaKid's award-winning Minecraft coding courses for ages 8+ provide the perfect way
  to learn Java programming with the professional text editor Eclipse! Kids make amazing custom mods 
  for Minecraft that they can share with family and friends!
slug: minecraft-coding
details:
  stats:
    - figure: 14
      stat: courses
    - figure: 98
      stat:
        quests
    - figure: 295
      stat: challenges
  benefits_promo:
        - image: /img/page_courses/common/sword.svg
          content: reate amazing custom tools, weapons, biomes, and more for Minecraft®!
        - image: /img/page_courses/common/web-development.svg
          content: Kids learn real Java coding using the Eclipse text editor
        - image: /img/page_courses/common/smile.svg
          content: Students learn conditionals, loops, booleans, methods, and more!
  benefits_promo_alt:
        - title: REAL WORLD SKILLS
          content: CodaKid students learn real programming languages and build real software using the 
            same tools that the pros use.
          image: /img/page_courses/common/rws.png
        - title: FUN AND ENGAGING
          content: CodaKid teaches coding the way kids like to be taught with youthful teachers and 
            YouTuber style.
          image: /img/page_courses/common/fae.png
        - title: KEEP YOUR PROJECTS FOREVER
          content: With CodaKid courses you get to keep the software, tools, and projects forever.
          image: /img/page_courses/common/kypt.png
  courses: 
    - title: Mod Creation Essentials
      cover: /img/page_courses/minecraft-coding/mod-creation-essentials.jpg
      difficulty: intro
      description: In this beginner level Minecraft coding course, kids learn real Java while making a
        custom axe that shoots fireballs as well as a repulsion shield that shoots enemies into the air!
        We cover key concepts such as conditionals and loops.
      chapters:
        - image: /img/page_courses/chapters/mod-creation-essentials.png
          title: get started
        - image: /img/page_courses/chapters/mod-creation-essentials/2.png
          title: command blocks
        - image: /img/page_courses/chapters/mod-creation-essentials/3.png
          title: starting our axe
        - image: /img/page_courses/chapters/mod-creation-essentials/4.png
          title: starting our shield
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Forge Your Sword"
      cover: /img/page_courses/minecraft-coding/forge-your-sword.jpg
      difficulty: beg
      description: In this popular course, students design their very own custom sword and give it
        special powers using Java programming. We deepen our understanding of variables and methods.
      chapters:
        - image: /img/page_courses/chapters/forge-your-sword/1.png
          title: Fire Up The Forge
        - image: /img/page_courses/chapters/forge-your-sword/2.png
          title: One Sword To Rule Them All 
        - image: /img/page_courses/chapters/forge-your-sword/3.png
          title: Revenge Of The Monster
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Power Ore"
      cover: /img/page_courses/minecraft-coding/power-ore.jpg
      difficulty: beg
      description: n this exciting Minecraft coding course, kids will create a super-powered pickaxe and 
        custom ore! We cover constructors and variables and deeper students' understanding of Java.
      chapters:
        - image: /img/page_courses/chapters/power-ore/1.png
          title: Preparing our Tools
        - image: /img/page_courses/chapters/power-ore/2.png
          title: To Smelt Ore not to Smelt 
        - image: /img/page_courses/chapters/power-ore/3.png
          title: It's Mine! All Mine! 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Create a Creature"
      cover: /img/page_courses/minecraft-coding/create-a-creature.jpg
      difficulty: beg
      description: In this highly creative Minecraft modding course, students will design and code their
        own custom mob in Minecraft! Students will deepen their understanding of parameters, variables, 
          and methods.
      chapters:
        - image: /img/page_courses/chapters/create-a-creature/1.png
          title: Preparing for an Epic Quest
        - image: /img/page_courses/chapters/create-a-creature/2.png
          title: IT'S ALIVE! 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Lucky Block"
      cover: /img/page_courses/minecraft-coding/lucky-block.jpg
      difficulty: int
      description: In this popular Minecraft coding course, students will design and code their own
        custom Lucky Block mod! Students will deepen their understanding of randomization and 
        conditionals.
      chapters:
        - image: /img/page_courses/chapter/lucky-block/1.png
          title: Preparing To Get Lucky 
        - image: /img/page_courses/chapter/lucky-block/2.png
          title: What's In The Box?? 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Magic Armor"
      cover: /img/page_courses/minecraft-coding/magic-armor.jpg
      difficulty: int
      description: In this action-packed course, students will design and code their own custom Magic
        Armor in Minecraft! Students will deepen their understanding of methods, variables, and 
        parameters.
      chapters:
        - image: /img/page_courses/chapter/magic-armor/1.png
          title: Fire Up The Forge
        - image: /img/page_courses/chapter/magic-armor/2.png
          title: The Glow Up 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Brand New Biomes"
      cover: /img/page_courses/minecraft-coding/brand-new-biomes.jpg
      difficulty: int
      description: In this highly creative Minecraft coding course, students will create their own
        custom biome! Students will deepen their understanding of methods, conditionals, and more!
      chapters:
        - image: /img/page_courses/chapter/new-biomes/1.png
          title: Fire Up The Forge
        - image: /img/page_courses/chapter/new-biomes/2.png
          title: Extreme Landscaping 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Epic Weapons"
      cover: /img/page_courses/minecraft-coding/epic-weapons.jpg
      difficulty: int
      description: In this popular Minecraft modding course, students will design and code a series of
        over the top weapons, including our infamous Cow Cannon!
      chapters:
        - image: /img/page_courses/chapter/epic-weapons/1.png
          title: Preparing your Weapons
        - image: /img/page_courses/chapter/epic-weapons/2.png
          title: Thunder Hammer 
        - image: /img/page_courses/chapter/epic-weapons/3.png
          title: Lava Launcher 
        - image: /img/page_courses/chapter/epic-weapons/4.png
          title: Bovine Bullets 
        - image: /img/page_courses/chapter/epic-weapons/5.png
          title: Wrap Up 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Build and Boom!"
      cover: /img/page_courses/minecraft-coding/build-and-boom.jpg
      difficulty: adv
      description: In this explosive coding course, students will design and code a custom structure and
        then create massive explosions using Java! Students will deepen their understanding of loops.
      chapters:
        - image: /img/page_courses/chapter/build-and-boom/1.png
          title: Prepare the Explosives!
        - image: /img/page_courses/chapter/build-and-boom/2.png
          title: Boom! 
        - image: /img/page_courses/chapter/build-and-boom/3.png
          title: Build and then BOOM!
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: House In A Box"
      cover: /img/page_courses/minecraft-coding/house-in-a-box.jpg
      difficulty: adv
      description: In this popular mod, students will design and code a house that appears instantly
        with code. Students will deepen their understanding of parameters, variables, array lists, and 
        loops.
      chapters:
        - image: /img/page_courses/chapter/house-in-a-box/1.png
          title: A House Needs A Foundation 
        - image: /img/page_courses/chapter/house-in-a-box/2.png
          title: The Transformation 
        - image: /img/page_courses/chapter/house-in-a-box/3.png
          title: It's Bigger On The Inside 
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Infinite Items 1.12"
      cover: /img/page_courses/minecraft-coding/infinite-items.jpg
      difficulty: adv
      description: In this creative course, students will learn to use json files to create an infinite
        number of items in Minecraft. Students will improve their understanding of data files.
      chapters: 
        - image: /img/page_courses/chapter/infinite-items/1.png
          title: Copy That
        - image: /img/page_courses/chapter/infinite-items/1.png
          title: The Item Factory 
        - image: /img/page_courses/chapter/infinite-items/1.png
          title: The Block Factory
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1: Artificial Intelligence and Fireballs"
      cover: /img/page_courses/minecraft-coding/ai_and_fireballs.png
      difficulty: adv
      description: In this fiery course, students will customize the AI of their creature (from Create A
        Creature) to create a fearsome fireball attack. Students will learn about Artificial 
        Intelligence and randomization.
      chapters:
        - image: /img/page_courses/chapter/ai_and_fireballs/1.png
          title:
        - image: /img/page_courses/chapter/ai_and_fireballs/2.png
          title:
        - image: /img/page_courses/chapter/ai_and_fireballs/3.png
          title:
      requirements:
        - title:
          image:
          points:
    - title: "Mod Creation 1 with Minecraft 1.8"
      cover: /img/page_courses/minecraft-coding/mod_creation_1_1.8.jpg
      difficulty: beg
      description: This Minecraft coding course is CodaKid's original Mod Creation classic and provides
        a whopping 35+ hours of award-winning student projects. We cover key concepts such as parameters,
        variables, conditional, loops, and arrays.
      chapters:
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/1.png
          title: Forge Your Sword 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/2.png
          title: Strike the Earth 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/3.png
          title: Power Armor 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/4.png
          title: Make Some Monsters 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/5.png
          title: Rare Loot 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/6.png
          title: Brand New Biomes 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/7.png
          title:  Explosions and Special Effects 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/8.png
          title:  Custom Structures 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/9.png
          title:  World Generation 
        - image: /img/page_courses/chapter/mod-creation-1-with-minecraft/10.png
          title: Epic Weapons 
      requirements:
        - title:
          image:
          points:
    - title: Mod Creation 2 with Minecraft 1.8
      cover: /img/page_courses/minecraft-coding/mod_creation_2_1.8.jpg
      difficulty: int
      description: This creative and captivating Minecraft coding course teaches students how to make 3
        custom dimensions in Minecraft. We cover more advanced methods, loops, and conditionals.
      chapters: 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/1.png
          title: The Overworld 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/2.png
          title: Crops 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/3.png
          title: The Underworld 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/4.png
          title: Creepers & Bombs 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/5.png
          title: Sky Island 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/6.png
          title: Pets & Lasers 
        - image: /img/page_courses/chapter/mod-creation-2-with-minecraft/7.png
          title: Wrap Up 
      requirements:
        - title:
          image:
          points:
---