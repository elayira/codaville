import React from 'react'
import {graphql} from 'gatsby'
import CoursePageTemplate from '../components/CoursePageTemplate'

export default ({data}) => {
  return (
    <CoursePageTemplate courseBundle={data} />
  )
}

export const CoursePageQuery = graphql`
  query CourseBundleByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
            slug
          }
      frontmatter {
        templateKey
        meta_description
        slug
        title
        details {
          stats {
            figure
            stat
          }
          courses {
            title
            cover
            difficulty
            description
          }
        }
      }
    }
  }
`
