import React from 'react'
import PropTypes from 'prop-types'
import {graphql} from 'gatsby'
import AboutPageTemplate from '../components/AboutPageTemplate'

const AboutPage = ({data}) => {
  const {frontmatter} = data.markdownRemark

  return (
    <div>
      <AboutPageTemplate
        title={frontmatter.title}
        meta_title={frontmatter.meta_title}
        meta_description={frontmatter.meta_description}
        about={frontmatter.about}
      />
    </div>
  )
}

AboutPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default AboutPage

export const aboutPageQuery = graphql`
  query AboutPage($id: String) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        meta_title
        meta_description
        about {
          core_values {
            value
          }
          what_we_do {
            title
            contents {
              content
            }
          }
          team {
            caption
            members {
              name
              title
              description
              image
            }
          }
        }
      }
    }
  }
`
