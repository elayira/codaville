import React from 'react'
import Helmet from 'react-helmet'
import Pricing from '../Pricing'
import PropTypes from 'prop-types'

const PricingPageTemplate = ({
  title,
  meta_title,
  meta_description,
  pricing,
}) => (
  <div>
    <Helmet
      htmlAttributes={{ lang: 'en' }}
      meta={[{ name: 'description', content: meta_description }]}
      title={`${meta_title}`}
      link={[{
        href: 'https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css',
        rel: 'stylesheet',
        type: 'text/css',
      }]}
      script={[{
        type: 'text/javascript',
        id: 'snipcart',
        'data-api-key': 'YmZlOGU4OWMtMDgwNy00MzM0LTg2MmItYWE2ZmQ3NmM5MjIzNjM2ODIyMjcxNTgwMDg4ODEw',
        src: 'https://cdn.snipcart.com/scripts/2.0/snipcart.js',
      }, {
        type: 'text/javascript',
        src: 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js',
      }]}
    />
    <section className='hero is-fullheight-with-navbar intro is-bold is-fullheigh'>
      <div className='hero-body'>
        <div className='container'>
          <div className='columns'>
            <div className='column is-10 is-offset-1'>
              <div className='section'>
                <h1 className='title box has-text-centered is-uppercase is-1'>
                  {title}
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div className='has-background-grey-lighter' style={{padding: '15px'}}>
      <div className='level'>
        <div className='level-item'>
          <article className='media'>
            <figure className='image is-96x96 media-left'>
              <img src='/img/gifts/gh1.png' className='is-inline-block' />
            </figure>
            <div className='media-content'>
              <div className='image is-96x96' style={{paddingTop: '20px'}}>
                <p className='title is-6'>No Coding Experience Necessary</p>
              </div>
            </div>
          </article>
        </div>
        <div className='level-item'>
          <article className='media'>
            <figure className='image is-96x96 media-left'>
              <img src='/img/gifts/gh2.png' className='is-inline-block' />
            </figure>
            <div className='media-content'>
              <div className='image is-96x96' style={{paddingTop: '20px'}}>
                <p className='title is-6'>Online Access To Awesome Teachers</p>
              </div>
            </div>
          </article>
        </div>
        <div className='level-item'>
          <article className='media'>
            <figure className='image is-96x96 media-left'>
              <img src='/img/gifts/gh3.png' className='is-inline-block' />
            </figure>
            <div className='media-content'>
              <div className='image is-96x96' style={{paddingTop: '20px'}}>
                <p className='title is-6'>Pro-Grade Programming Tools</p>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
    <section className='section section--gradient'>
      <div className='container'>
        <div className='section'>
          <div className='columns'>
            <div className='column is-10 is-offset-1'>
              <div className='has-text-centered'>
                <h2 className='has-text-info has-text-weight-semibold is-size-2'>
                  {pricing.heading}
                </h2>
                <p className='is-size-5 section box has-background-primary has-text-white'>{pricing.description}</p>
                <Pricing data={pricing.plans} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
)

PricingPageTemplate.propTypes = {
  title: PropTypes.string,
  meta_title: PropTypes.string,
  meta_description: PropTypes.string,
  pricing: PropTypes.shape({
    heading: PropTypes.string,
    description: PropTypes.string,
    plans: PropTypes.array,
  }),
}

export default PricingPageTemplate
