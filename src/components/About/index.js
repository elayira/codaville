import React from 'react'
import PropTypes from 'prop-types'

const About = ({ about }) => (
  <section className='section'>
    <div className='container is-fluid'>
      <p className='title box is-uppercase is-inline-block'>{about.what_we_do.title}</p>
      <div className='columns'>
        {about.what_we_do.contents.map((item, index) => (
          <div className='column is-half' key={index}>
            <p className='content'>{item.content}</p>
          </div>
        ))}
      </div>
      <div className='container is-fluid'>
        <div className='has-text-centered section'>
          <p className='title box is-uppercase is-inline-block'>{about.team.caption}</p>
        </div>
        {about.team.members.map((profile, index) => {
          const isEven = digit => digit % 2 === 0
          if (isEven(index)) {
            return (
              <div key={profile.name} className='columns is-gapless is-vcentered'>
                <div className='column is-half'>
                  <figure className='image'>
                    <img src={profile.image} />
                  </figure>
                </div>
                <div className='column card is-half has-background-warning'>
                  <div className='card-content'>
                    <p
                      className='title is-radiusless box is-5 has-text-centered'
                      style={{position: 'relative', left: '-25px'}}
                    >
                      {`${profile.name}, ${profile.title}`}
                    </p>
                    <p className='has-text-white'>{profile.description}</p>
                  </div>
                </div>
              </div>
            )
          } else {
            return (
              <div key={profile.name} className='columns is-gapless is-vcentered'>
                <div className='column card is-half has-background-warning'>
                  <div className='card-content'>
                    <p
                      className='title is-radiusless box is-5 has-text-centered'
                      style={{position: 'relative', left: '25px'}}
                    >
                      {`${profile.name}, ${profile.title}`}</p>
                    <p className='has-text-white'>{profile.description}</p>
                  </div>
                </div>
                <div className='column is-half'>
                  <figure className='image'>
                    <img src={profile.image} />
                  </figure>
                </div>
              </div>
            )
          }
        })}
      </div>
      <section className='section'>
        <div className='container is-fluid'>
          <div className='has-text-centered section'>
            <p className='title box is-uppercase is-inline-block'>our core values</p>
          </div>
          <div className='columns is-multiline is-vcentered'>
            {about.core_values.map((principle, index) => (
              <div className='column is-half' key={index}>
                <div className='box has-background-primary has-text-grey-lighter'>
                  <article className='media'>
                    <figure className='media-left'>
                      <p style={{fontSize: '5em'}}>
                        {1 + index}
                      </p>
                    </figure>
                    <div className='media-content'>
                      <div className='content'><p className='title has-text-white'>{principle.value}</p></div>
                    </div>
                  </article>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </div>
  </section>
)

About.propTypes = {
  about: PropTypes.shape({
    what_we_do: PropTypes.object,
    team: PropTypes.object,
  }),
}

export default About
