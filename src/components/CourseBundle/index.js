import React from 'react'
import { StaticQuery, graphql, Link } from 'gatsby'

import ProgessCard from '../CourseCards/ProgressCard'

export default () => (
  <StaticQuery
    query={graphql`
      {
        allMarkdownRemark(
          limit: 50,
          filter: {frontmatter: {templateKey: {eq: "course-page"}}},
          sort: {order: DESC, fields: [frontmatter___date]})
          {
          edges {
            node {
              frontmatter {
                templateKey
                slug
                title
                details {
                  courses {
                    title
                    cover
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => {
      const { edges } = data.allMarkdownRemark
      const colors = ['choose-us-banner', 'generic-banner']
      return (
        edges.map(({node: courseBundle}) => (
          <div
            className={`has-text-centered section ${colors[Math.floor(Math.random() * colors.length)]}`}
            key={courseBundle.frontmatter.title}
          >
            <ProgessCard courseBundle={courseBundle.frontmatter} />
            <Link
              className='button is-large is-primary is-capitalized'
              to={`course-bundle/${courseBundle.frontmatter.slug}`}
              style={{margin: '15px'}}
            >
              {`Check out ${courseBundle.frontmatter.title} For Kids!`}
            </Link>
          </div>
        ))
      )
    }}
  />
)
