import React from 'react'
import PropTypes from 'prop-types'
import BuyButton from '../BuyButton'

const Pricing = ({ data }) => (
  <div className='columns is-multiline'>
    {data.map(course => (
      <div key={course.plan} className='column is-half'>
        <section className='card'>
          <div className='card-header has-background-info'>
            <h4 className='is-centered card-header-title has-text-centered has-text-white has-text-weight-semibold'>
              {course.plan}
            </h4>
          </div>
          <a
            id={course.id}
            href='#'
            className='snipcart-add-item'
            data-item-id={course.id}
            data-item-price={course.price}
            data-item-image={course.image}
            data-item-name={course.plan}
            data-item-description={course.description}
            data-item-url={'https://prod-codaville.netlify.com/' + course.path}
          >
            <div className='card-image'>
              <figure className='image'>
                <img src={course.image} />
              </figure>
            </div>
          </a>

          <div className='card-content'>
            <h2 className='is-size-1 has-text-weight-bold has-text-primary has-text-centered box'>
              <a
                id={course.id}
                href='#'
                className='snipcart-add-item has-text-primary'
                data-item-id={course.id}
                data-item-price={course.price}
                data-item-image={course.image}
                data-item-name={course.plan}
                data-item-description={course.description}
                data-item-url={'https://prod-codaville.netlify.com/' + course.path}
              >
              ${course.price}
              </a>
            </h2>
            <p>{course.description}</p>
          </div>
          <div className='card-footer' style={{borderTop: 'none'}}>
            <div className='card-footer-item' style={{paddingBottom: '0px'}}>
              <BuyButton course={course} />
            </div>
          </div>
        </section>
      </div>
    ))}
  </div>
)

Pricing.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      plan: PropTypes.string,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      description: PropTypes.string,
      image: PropTypes.string,
      id: PropTypes.string,
    })
  ),
}

export default Pricing
